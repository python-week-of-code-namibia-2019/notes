# Python Week Of Code, Namibia 2019

## Installation

### Python

First check whether your computer is running a 32-bit version or a 64-bit version of Windows at https://support.microsoft.com/en-au/kb/827218.
You can download Python for Windows from the website https://www.python.org/downloads/.

[Video](videos/install-python.webm)

### Django

On PowerShell,
run

```
python -m pip install Django==2.2.4
```

or

```
pip install django==2.2.4
```

[Video](videos/install-django.webm)

### Git

You can download Git for Windows from the website http://git-scm.com/.

[Video](videos/install-git.webm)

### Code Editor

There are a lot of different editors and it largely boils down to personal preference.
In this notes, we will use [Visual Studio Code](https://code.visualstudio.com/).

[Video](videos/install-visual-studio.webm)

## Friday, 23 (9:00 - 11:00)

How the internet works

### HTML

### CSS

### JS

### Python

### Model–View–Controller architectural pattern

https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller

## Friday, 23 (11:30 - 13:30)

View

### HTML

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-23-view-html

### CSS

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-23-view-css

### JS

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-23-view-js

### Template

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-23-view-template

## Friday, 23 (14:00 - 16:00)

Model (1)

### Creating models

### Python shell

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-23-model-title

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-23-model-save

### Django Admin

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-23-model-admin

## Friday, 23 (16:30 - 18:00)

Model (2)

### Migration

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-23-model-migration

### User

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-23-model-user

## Saturday, 24 (9:00 - 11:00)

Controller (1)

### URL

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-24-controller-url

### View

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-24-controller-view

## Saturday, 24 (11:30 - 13:30)

Controller (2)

### View

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-24-controller-404

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-24-controller-list

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-24-controller-api

### Querysets

## Saturday, 24 (14:00 - 16:00)

Git & GitHub

## Saturday, 24 (16:30 - 18:00)

Projects

## Sunday, 25 (9:00 - 11:00)

Forms (1)

### Simple form

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-25-form-basic

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-25-form-from-classes

## Sunday, 25 (11:30 - 13:30)

Forms (2)

### Forms from models

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-25-form-from-models

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-25-form-edit

## Sunday, 25 (14:00 - 16:00)

CSS (1)

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-25-css-basic

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-25-css-bulma

## Sunday, 25 (16:30 - 18:00)

Projects

## Monday, 26 (9:00 - 11:00)

Testing (1)

### Create a test

### Running tests

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-26-test-unittest

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-26-test-django

## Monday, 26 (11:30 - 13:30)

Testing (2)

### Continuous Integration

https://gitlab.com/python-week-of-code-namibia-2019/2019-08-26-test-ci

## Monday, 26 (14:00 - 16:00)

CSS (2)

## Monday, 26 (16:30 - 18:00)

Projects

## Tuesday, 27 (9:00 - 11:00)

Javascript (1)

## Tuesday, 27 (11:30 - 13:30)

Javascript (2)

## Tuesday, 27 (14:00 - 16:00)

Deploy

### PythonAnywhere

## Tuesday, 27 (16:30 - 18:00)

Projects

## Wednesday, 28 (9:00 - 11:00)

Competition

## Wednesday, 28 (11:30 13:30)

Project Presentation

## Wednesday, 28 (14:00 - 16:00)

Project Presentation

## Wednesday, 28 (16:30 - 18:00)

Close
